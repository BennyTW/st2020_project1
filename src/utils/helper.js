
let sorting = (array) => {	
    return array.sort();
}

let compare = (a, b) => {
	if(a['PM2.5']>b['PM2.5']){
		return 1;
	}else{
		return -1;
	}
	// console.log(a);
	// console.log(b);
 //    return 1;
}

let average = (nums) => {

	var sum = 0;
	for( var i = 0; i < nums.length; i++ ){
	    sum += nums[i]
	}
	var avg = sum/nums.length;
	return +(Math.round(avg + "e+2")  + "e-2");
}


module.exports = {
    sorting,
    compare,
    average
}